/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MuonEtaHoughTransformAlg.h"
#include "../MuonPhiHoughTransformAlg.h"
DECLARE_COMPONENT(MuonR4::MuonEtaHoughTransformAlg)
DECLARE_COMPONENT(MuonR4::MuonPhiHoughTransformAlg)
